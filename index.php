<?php require_once "./code.php" ?>


<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>PHP SC S01 Activity</title>
</head>
<body>
	
	<h1>Full Address:</h1>
	<p><?php echo getFullAddress('Philippines', "Cebu City", "Cebu", "123-A Padilla St."); ?></p>
	<p><?php echo getFullAddress('Philippines', "Cebu City", "Cebu", "456-B Rodriguez St."); ?></p>

	<h1>Letter-Based Grading</h1>
	<p><?php echo getLetterGrade(97) ?></p>
	<p><?php echo getLetterGrade(82) ?></p>
	<p><?php echo getLetterGrade(75) ?></p>

</body>
</html>